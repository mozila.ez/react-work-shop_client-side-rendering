import React, { Component } from 'react';
import Header from '../components/Header';
import Monitor from '../components/monitor/Monitor';
import Footer from '../components/Footer';
import { connect } from 'react-redux';
import { productsFetch } from '../actions/index';
// import axios from 'axios';

class Home extends Component {

  constructor(props){
    super(props);
    // this.state = {products : ""};

  }

  componentDidMount(){
    /* V.1
    this.setState({products:[
      { id: 1, productName: "สลัดผัก", unitPrice: "120", thumbnail: "/images/product/1.jpg" },
      { id: 2, productName: "ไก่ทอด", unitPrice: "90", thumbnail: "/images/product/2.jpg" },
      { id: 3, productName: "บิงซู", unitPrice: "200", thumbnail: "/images/product/3.jpg" },
      { id: 4, productName: "เฟรนฟราย", unitPrice: "140", thumbnail: "/images/product/4.jpg" },
      { id: 5, productName: "เค้ก 3 ชั้น", unitPrice: "200", thumbnail: "/images/product/5.jpg" },
      { id: 6, productName: "กาแฟ เฮลตี้ฟู้ด", unitPrice: "140", thumbnail: "/images/product/6.jpg" }
  ]})
    */
    // V.2
    // fetch("http://localhost:3001/products", {method : "GET"})
    //   .then(res => res.json())
    //   .then(res => {this.setState({products : res})});

    // V.3
    // axios.get("http://localhost:3001/products").then(res => {
    //   {this.setState({products:res.data})}
    // });
  
    //Redux 
    this.props.productsFetch();

   }

  render() {
    return (
      <div>
       <Header />
       {this.props.products && Array.isArray(this.props.products) && (
          <Monitor products={this.props.products}/>
       )}
       <Footer company="Mozilaez" email="mozila.ez@gmail.com"/>
      </div>
    );
  }
}

function mapStateToProps({ products }){
  // console.log(state);
  return { products };
}

export default connect(mapStateToProps,{productsFetch})(Home);
